<?php 
include "includes/global/db_connexion.php"; 
include "includes/global/functions.php";
include "includes/global/functions_rss.php";

// Create connection
$con=mysqli_connect($host, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno($con)) {
  echo "Database connection failed!: " . mysqli_connect_error();
}
$con->query("SET NAMES 'utf8'"); 
$con->query("SET CHARACTER SET utf8");  
$con->query("SET SESSION collation_connection = 'utf8_unicode_ci'");

$sql = "SELECT * FROM veilles ORDER BY id DESC LIMIT 20";
$query = mysqli_query($con,$sql);
//  debug(mysqli_fetch_array($query, MYSQLI_ASSOC),1);
header('Content-Type: application/rss+xml'); 
 echo "<?xml version='1.0' encoding='UTF-8'?><rss version='2.0'><channel><title>w3schools.in | RSS</title><link>/</link><synthese>Cloud RSS</synthese><language>fr</language>";
 
 while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {   
   $title=($row["sujet"]);
   $link=$row["liens"];
   $synthese=($row["synthese"]);
   $pubDate=$row["date"];
   
   echo "<item><title><![CDATA[ $title ]]></title><link>$link</link><synthese><![CDATA[ $synthese ]]></synthese><pubDate>$pubDate</pubDate></item>";
 }
 echo "</channel></rss>";
?>