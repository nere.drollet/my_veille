<?php 
include "includes/global/db_connexion.php"; 
include "includes/global/functions.php";
include "includes/global/select_veille.php";
include "includes/global/pagination.php";
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyVeille</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/article.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body id="img">
    <div class="container">
        <div class="row">
            <div class="column small-12 large-9 article-list-wrapper-borfer is-relative cc_cursor">
                <?php
                for ($i=0; $i < count($showVeille); $i++) {
                ?>
                <div class="article-list is-relative cc-cursor" style="margin: 2em;">
                    <article class=" article article-big block-grid small-collapse is-relative cc_cursor">
                        <div class="column small-12 medium-5 large-6 is-relative">
                            <div class="article-image-wrapper">
                                <img src="<?php echo utf8_encode($showVeille[$i]['images']); ?>" id="wrap" alt="">
                                <div id="float">
                                    <label for="exampleFormControlTextarea1" class="form-label">Synthèse</label>
                                    <p id="synthese">
                                        <?php echo utf8_encode($showVeille[$i]['synthese']); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- <button onclick="openveille()">Veille</button> -->
                        <div class="column small-12 mediu-7 large-6">
                            <div style="position: relative; padding: 15px 15px 0;">
                                <h2 class="h3-size" style="margin-top: 2em;">
                                    <?php echo utf8_encode($showVeille[$i]['sujet']); ?>
                                </h2>
                                <div style="margin-top: 2em;">
                                    <label for="exampleFormControlTextarea1" class="form-label">Commentaires</label>
                                    <p id="comment">
                                        <?php echo utf8_encode($showVeille[$i]['commentaires']); ?>
                                    </p>
                                </div>
                                <div style="color: black; margin-top: 2em;">
                                    <a href="<?php echo utf8_encode($showVeille[$i]['liens']);?>" style="color:blue;">
                                        SOURCE
                                    </a>
                                    <h2>
                                        <?php echo $showVeille[$i]['date']; ?>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php 
                } 
                ?>
                <nav>
                    <ul class="pagination" style="margin-left: 23em; margin-bottom: 2em;">
                        <!-- Lien vers la page précédente (désactivé si on se trouve sur la 1ère page) -->
                        <li class="page-item <?= ($currentPage == 1) ? "disabled" : "" ?>">
                            <a href="./?page=<?= $currentPage - 1 ?>" class="page-link">Précédente</a>
                        </li>
                        <?php for($page = 1; $page <= $pages; $page++): ?>
                        <!-- Lien vers chacune des pages (activé si on se trouve sur la page correspondante) -->
                        <li class="page-item <?= ($currentPage == $page) ? "active" : "" ?>">
                            <a href="./?page=<?= $page ?>" class="page-link"><?= $page ?></a>
                        </li>
                        <?php endfor ?>
                        <!-- Lien vers la page suivante (désactivé si on se trouve sur la dernière page) -->
                        <li class="page-item <?= ($currentPage == $pages) ? "disabled" : "" ?>">
                            <a href="./?page=<?= $currentPage + 1 ?>" class="page-link">Suivante</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>
</body>

</html>