<?php
// On détermine sur quelle page on se trouve
if(isset($_GET['page']) && !empty($_GET['page'])){
    $currentPage = (int) strip_tags($_GET['page']);
}else{
    $currentPage = 1;
}


// On détermine le nombre total d'articles
$sql = 'SELECT COUNT(*) AS veilles FROM `veilles`;';

// On prépare la requête
$req = $dbh->query($sql);

// On exécute
$req->execute();

// On récupère le nombre d'articles
$result = $req->fetch();

$nbArticles = (int) $result['veilles'];

// On détermine le nombre d'articles par page
$parPage = 5;

// On calcule le nombre de pages total
$pages = ceil($nbArticles / $parPage);

// Calcul du 1er article de la page
$premier = ($currentPage * $parPage) - $parPage;

$sql = 'SELECT * FROM `veilles` ORDER BY `veilles`.`date` DESC LIMIT :premier, :parpage;';

// On prépare la requête
$req = $dbh->prepare($sql);

$req->bindValue(':premier', $premier, PDO::PARAM_INT);
$req->bindValue(':parpage', $parPage, PDO::PARAM_INT);

// On exécute
$req->execute();

// On récupère les valeurs dans un tableau associatif
$showVeille = $req->fetchAll(PDO::FETCH_ASSOC);

require_once('close.php');
?>