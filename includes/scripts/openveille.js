function openveille(){
    var fromId = localStorage.getItem("id");
    var sujet = $("sujet").val();
    var lien = $("liens").val();
    var synthese = $("synthese").val();
    var comment = $("commentaires").val();
    var images = $("images").val();
    $.ajax({
        url: "includes/veille.php",
        type: "POST",
        data: {
            fromId: fromId,
            sujet: sujet,
            lien: lien,
            synthese: synthese,
            comment: comment,
            images: images
        },
        success: function(data){
            const result = JSON.parse(data);
            console.log("myComment", result);
        }
    });
} 